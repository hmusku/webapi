﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using static System.Configuration.ConfigurationManager;

namespace WebAPIQA.Attributes
{
    public class ApiKeyAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var request = actionContext.Request;
            var isValidApiKey = false;
            IEnumerable<string> lsHeaders;
            //Validate that the api key exists

            var checkApiKeyExists = request.Headers.TryGetValues("API_KEY", out lsHeaders);

            if (checkApiKeyExists)
            {
                // ReSharper disable once PossibleNullReferenceException
                if (lsHeaders.FirstOrDefault().Equals(AppSettings["ApiKey"]))
                {
                    isValidApiKey = true;
                }
            }

            //If the key is not valid, return an http status code.
            if (!isValidApiKey)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized,"Unauthorized Access");
            }
        }
    }
}