﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPIQA.Models;

namespace WebAPIQA.Controllers
{
    public class ValuesController : ApiController
    {
        /// <summary>
        /// This method uses to Get 
        /// </summary>
        /// <returns></returns>
        // GET api/values
        [ApiExplorerSettings(IgnoreApi = true)]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        /// <summary>
        /// This method uses to Get 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        // GET api/values/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public string Get(int id)
        {
         
            return "value";
        }

        // POST api/values
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Delete(int id)
        {
        }
    }
}
