﻿using log4net;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web.Http;
using WebAPIQA.Attributes;
using WebAPIQA.Models;
using WebAPIQA.Services;

namespace WebAPIQA.Controllers
{

    [ApiKeyAuthorize]
    [RoutePrefix("api/v1")]
    public class QuestionController : ApiController
    {
        ILog logger = log4net.LogManager.GetLogger("LogFileAppender");

        private IQuestions _QuestService { get; set; }

        public QuestionController(IQuestions QuestService)
        {
            _QuestService = QuestService;
        }

        /// <summary>
        /// Create a Question
        /// </summary>
        /// <param name="objQuestionData"></param>
        /// <returns>Success</returns>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Type = typeof(string))]
        [SwaggerResponse((int)HttpStatusCode.Unauthorized, Type = typeof(string))]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, Type = typeof(string))]
        public IHttpActionResult PostQuestion(QuestionModel objQuestionData)
        {
            var validModelAttrMsg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    // if (objQuestionData.OrgID == 0) { validModelAttrMsg = "OrgID"; }
                    if (objQuestionData.LotID == 0) { validModelAttrMsg = validModelAttrMsg + " " + "LotID"; }
                    if (string.IsNullOrEmpty(objQuestionData.Question)) { validModelAttrMsg = validModelAttrMsg + " " + "Question"; }
                    if (!objQuestionData.QuestionDateTime.HasValue) { validModelAttrMsg = validModelAttrMsg + " " + "QuestionDateTime"; }
                    if (objQuestionData.EMID == 0) { validModelAttrMsg = validModelAttrMsg + " " + "EMID"; }
                    if (objQuestionData.copyBuyer == null) { validModelAttrMsg = validModelAttrMsg + " " + "copyBuyer"; }
                    if (!string.IsNullOrWhiteSpace(validModelAttrMsg)) { return BadRequest("Invalid " + validModelAttrMsg + " data has been passed"); }


                    QuestionsModel objQuestion = new QuestionsModel();
                    // objQuestion.OrgID = objQuestionData.OrgID;
                    objQuestion.LotID = objQuestionData.LotID;
                    objQuestion.Question = objQuestionData.Question;
                    objQuestion.QuestionDateTime = objQuestionData.QuestionDateTime;
                    objQuestion.copyBuyer = objQuestionData.copyBuyer;
                    objQuestion.EMID = objQuestionData.EMID;

                    if (_QuestService.ValidateLotId(objQuestion.LotID))
                    {
                        int QuestionIdentity = _QuestService.PostQuestion(objQuestion);

                        if (QuestionIdentity > 0)
                        {
                            _QuestService.SendQuestionEmail(objQuestion);
                            return Ok("Success") as IHttpActionResult;
                        }
                        else
                            return BadRequest("Failed") as IHttpActionResult;
                    }
                    else
                    {
                        return BadRequest("Failed LotId doesn't exist") as IHttpActionResult;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("PostQuestion " + ex.Message);
                return InternalServerError(ex) as IHttpActionResult;

            }

            validModelAttrMsg = string.Empty;
            // if (objQuestionData.OrgID == 0) { validModelAttrMsg = "OrgID"; }
            if (objQuestionData.LotID == 0) { validModelAttrMsg = validModelAttrMsg + " " + "LotID"; }
            if (string.IsNullOrEmpty(objQuestionData.Question)) { validModelAttrMsg = validModelAttrMsg + " " + "Question"; }
            if (!objQuestionData.QuestionDateTime.HasValue) { validModelAttrMsg = validModelAttrMsg + " " + "QuestionDateTime"; }
            if (objQuestionData.EMID == 0) { validModelAttrMsg = validModelAttrMsg + " " + "EMID"; }
            if (objQuestionData.copyBuyer == null) { validModelAttrMsg = validModelAttrMsg + " " + "copyBuyer"; }
            if (!string.IsNullOrWhiteSpace(validModelAttrMsg)) { return BadRequest("Invalid " + validModelAttrMsg + " data has been passed"); }


            return BadRequest(string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage)));
        }

    }
}