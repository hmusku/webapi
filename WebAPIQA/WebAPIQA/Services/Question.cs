﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using WebAPIQA.Models;
using WebAPIQA.Repository;

namespace WebAPIQA.Services
{
    public class Question:IQuestions
    {
        ILog logger = log4net.LogManager.GetLogger("LogFileAppender");
        private IQuestionRepo _QuestionRepo { get; set; }

        public Question(IQuestionRepo QuestionRepo)
        {
            _QuestionRepo = QuestionRepo;
        }

        public int PostQuestion(QuestionsModel objQuestion)
        {
            try
            {
                string query = string.Format("select * from dbo.asset_auction where  LOT_LOT_ID={0}", objQuestion.LotID);
                DataTable dbasset_auction = _QuestionRepo.ExecuteQueryCommand(query);
                if (dbasset_auction.Rows.Count > 0)
                {
                    objQuestion.acct_id = Convert.ToInt32(dbasset_auction.Rows[0]["acct_id"]);
                    objQuestion.asset_id = Convert.ToInt32(dbasset_auction.Rows[0]["asset_id"]);
                }

                //objQuestion.asset_id = 1;
                //else
                //{
                //    return BadRequest("Failed LotId doesn't exist") as IHttpActionResult;
                //}
                query = string.Format("select * from dbo.buyers where EM_id = {0} ", objQuestion.EMID);
                DataTable dbBuyer = _QuestionRepo.ExecuteQueryCommand(query);

                if (dbBuyer.Rows.Count > 0)
                {
                    objQuestion.buyer_id = Convert.ToInt32(dbBuyer.Rows[0]["buyer_id"]);
                }

                query = string.Format("insert into asset_QandA ( acct_id,asset_id,buyer_id,question,question_date) values({0},{1},{2},'{3}','{4}');SELECT CAST(scope_identity() AS int)", objQuestion.acct_id, objQuestion.asset_id, objQuestion.buyer_id, objQuestion.Question, objQuestion.QuestionDateTime.ToString());

                int QuestionIdentity = _QuestionRepo.ExecuteNonQueryCommand(query);
                objQuestion.qID = QuestionIdentity;
                return QuestionIdentity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendQuestionEmail(QuestionsModel objQuestion)
        {
            try
            {

                //get Question based on qID
                string questionQuery = string.Format("select * from asset_QandA where qID = {0}", objQuestion.qID);
                DataTable dtgetQuestion = _QuestionRepo.ExecuteQueryCommand(questionQuery);
                List<string> toEmailList = new List<string>();
                List<string> tableToList = new List<string>();
                List<string> ccEmailList = new List<string>();
                List<string> tableCCList = new List<string>();
                string pocQuery = string.Empty;
                string assetQuery = string.Empty;
                string adminQuery = string.Empty;
                string AcctPocQuery = string.Empty;
                string assetQAquery = string.Empty;
                string bidderQuery = string.Empty;
                string status = string.Empty;

                if (dtgetQuestion.Rows.Count > 0)
                {
                    //check if POC exist for this asset

                    pocQuery = string.Format("SELECT a.email, id FROM subscriber_info a WITH(NOLOCK)   INNER JOIN asset_id b WITH(NOLOCK) on a.acct_id = b.acct_id AND a.id = b.poc   WHERE b.acct_id = {0}    AND b.asset_id = {1}  AND a.user_lock = 'U'", Convert.ToInt32(dtgetQuestion.Rows[0]["acct_id"]), Convert.ToInt32(dtgetQuestion.Rows[0]["asset_id"]));


                    DataTable dtgetPOC = _QuestionRepo.ExecuteQueryCommand(pocQuery);
                    //if there is a POC associated with this asset he it the 'TO' email receive
                    if (dtgetPOC.Rows.Count > 0)
                    {
                        toEmailList.Add(dtgetPOC.Rows[0]["email"].ToString());
                        tableToList.Add(dtgetPOC.Rows[0]["id"].ToString());
                    }


                    assetQuery = string.Format("SELECT ai.asset_id, ai.asset_short_desc, aa.asset_auction_end_dt, ai.asset_filler_1 FROM asset_id ai WITH(NOLOCK) INNER JOIN asset_auction aa ON ai.acct_id = aa.acct_id AND ai.asset_id = aa.asset_id and aa.auction_id = (SELECT MAX(auction_id) FROM asset_auction WHERE acct_id = {0}  and asset_id ={1})    WHERE ai.acct_id = {0}   and ai.asset_id = {1} ", dtgetQuestion.Rows[0]["acct_id"], dtgetQuestion.Rows[0]["asset_id"]);

                    DataTable dtgetAssetInfo = _QuestionRepo.ExecuteQueryCommand(assetQuery);

                    //if admin wants emailed
                    adminQuery = string.Format("SELECT email, id FROM subscriber_info WITH(NOLOCK) WHERE acct_id ={0} AND((security_level = 1 and(emailQa = 1 or emailQa is null)) OR(security_level not in (1, 3, 6) and emailQa = 1))  " + (toEmailList.Count > 0 ? "AND email!=" + toEmailList[0] : "") + "  AND user_lock = 'U'", dtgetQuestion.Rows[0]["acct_id"]);

                    System.Data.DataTable getAdmin = _QuestionRepo.ExecuteQueryCommand(adminQuery);

                    if (toEmailList.Count == 0)
                    {
                        foreach (System.Data.DataRow dr in getAdmin.Rows)
                        {
                            toEmailList.Add(dr["email"].ToString());
                            tableToList.Add(dr["id"].ToString());
                        }


                        if (toEmailList.Count == 0)
                        {
                            AcctPocQuery = string.Format("SELECT EMAIL FROM agency_acct WHERE ACCT_ID={0}", dtgetQuestion.Rows[0]["acct_id"]);
                            System.Data.DataTable GetAcctPoc = _QuestionRepo.ExecuteQueryCommand(AcctPocQuery);
                            foreach (System.Data.DataRow dr in GetAcctPoc.Rows)
                            {
                                toEmailList.Add(dr["email"].ToString());
                            }
                        }
                    }
                    else
                    {
                        foreach (System.Data.DataRow dr in getAdmin.Rows)
                        {
                            ccEmailList.Add(dr["email"].ToString());
                            tableCCList.Add(dr["id"].ToString());
                        }
                    }

                    //get bidder name

                    bidderQuery = string.Format(" Select username, email FROM buyers WITH(NOLOCK) WHERE buyer_id ={0}", dtgetQuestion.Rows[0]["buyer_id"]);

                    DataTable getBidder = _QuestionRepo.ExecuteQueryCommand(bidderQuery);

                    string newQuestion = dtgetQuestion.Rows[0]["question"].ToString();

                    string subject = string.Format("AuctionDeals Question Regarding - Asset {0} {1} ", dtgetQuestion.Rows[0]["asset_id"].ToString(), dtgetAssetInfo.Rows[0]["asset_short_desc"].ToString());

                    StringBuilder htmlBody = new StringBuilder();
                    htmlBody.AppendFormat("You have received a question from {0} regarding Asset {1}- {2}.", getBidder.Rows[0]["username"], dtgetQuestion.Rows[0]["asset_id"].ToString(), dtgetAssetInfo.Rows[0]["asset_short_desc"].ToString());
                    htmlBody.AppendLine(" <br/><br/>");
                    htmlBody.AppendLine("Please <strong><a href=''>log in</a></strong> to your account and reply to the question.");
                    htmlBody.AppendLine(" <br/><br/>");
                    htmlBody.AppendFormat(" <i>{0}</i> ", newQuestion);
                    htmlBody.AppendLine(" <br/><br/>");
                    htmlBody.AppendFormat("This question will no longer be available to answer after the auction closes, which is currently <strong> {0}</strong>.", dtgetAssetInfo.Rows[0]["asset_auction_end_dt"].ToString());
                    htmlBody.AppendLine(" <br/><br/>");
                    htmlBody.AppendLine(" Thank you,<br/>");
                    htmlBody.AppendLine(" AuctionDeals Support");
                    htmlBody.AppendLine(" <br/><br/>");

                    htmlBody.AppendFormat("QAL: {0}-{1}", dtgetQuestion.Rows[0]["acct_id"], dtgetAssetInfo.Rows[0]["asset_id"]);
                    if (dtgetAssetInfo.Rows[0]["asset_filler_1"].ToString() != "")
                        htmlBody.AppendLine(" <br/><br/>");
                    if (!string.IsNullOrEmpty(dtgetAssetInfo.Rows[0]["asset_filler_1"].ToString()))
                    {
                        htmlBody.AppendFormat("Inventory ID:  {0}", dtgetAssetInfo.Rows[0]["asset_filler_1"]);
                    }
                    SendEmail(string.Join(",", toEmailList), string.Join(",", ccEmailList), subject, htmlBody.ToString(), "Seller");

                    assetQAquery = string.Format(" UPDATE asset_QandA SET originalEmailTo = '{0}' " + (string.Join(",", tableCCList) != "" ? ", originalEmailCC ='" + string.Join(", ", tableCCList) + "'" : "") + " WHERE qID ={1}; ", string.Join(",", tableToList), objQuestion.qID);

                    int roweffect = _QuestionRepo.ExecuteNonQueryCommand(assetQAquery);

                    if (objQuestion.copyBuyer == true)
                    {
                        htmlBody = new StringBuilder();
                        subject = string.Format("AuctionDeals Question Regarding - Asset {0} ", dtgetAssetInfo.Rows[0]["asset_short_desc"].ToString());
                        htmlBody.AppendLine("The question below has been forwarded to the seller:");
                        htmlBody.AppendLine(" <br/><br/>");
                        htmlBody.AppendFormat("<i>{0}</i>", newQuestion);
                        htmlBody.AppendLine(" <br/><br/>");
                        htmlBody.AppendLine("You will receive an email after the question has been answered.");
                        htmlBody.AppendLine(" <br/><br/>");
                        htmlBody.AppendLine("Thank you,<br/>");
                        htmlBody.AppendLine("AuctionDeals Support");
                        SendEmail(getBidder.Rows[0]["email"].ToString(), string.Empty, subject, htmlBody.ToString(), "Buyer");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("SendQuestionEmail  " + ex.Message);
            }
        }


        private void SendEmail(string ToAddress, string CCAddresss, string subject, string htmlBody, string typeofEmail)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("gdsmtp.lsi.local");
                SmtpServer.UseDefaultCredentials = true;
                mail.From = new MailAddress("no-reply@auctiondeals.com");
                mail.To.Add(ToAddress);
                mail.Subject = subject;
                mail.Body = htmlBody;
                mail.IsBodyHtml = true;
                SmtpServer.Port = 25;
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                logger.Error("SendEmail Email Type is : " + typeofEmail + " " + ex.Message);
            }
        }

        public bool ValidateLotId(decimal LotID)
        {
            string query = string.Format("select * from dbo.asset_auction where  LOT_LOT_ID={0}", LotID);
            DataTable dbasset_auction = _QuestionRepo.ExecuteQueryCommand(query);
            if (dbasset_auction.Rows.Count == 0)
            {
                return false;
            }
            return true;
        }
    }
}