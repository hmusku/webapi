﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPIQA.Models;

namespace WebAPIQA.Services
{
  public interface IQuestions
    {
        int PostQuestion(QuestionsModel objQuestion);
        void SendQuestionEmail(QuestionsModel objQuestion);
        bool ValidateLotId(decimal LotID);
    }
}
