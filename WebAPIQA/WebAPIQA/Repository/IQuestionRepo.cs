﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace WebAPIQA.Repository
{
    public interface IQuestionRepo
    {
        DataTable ExecuteQueryCommand(string Query);
        int ExecuteNonQueryCommand(string Query);
    }
}