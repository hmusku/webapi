﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Odbc;

namespace WebAPIQA.Repository
{
    public class QuestionRepo: IQuestionRepo
    {
        private IDbConnection getConnection()
        {
            IDbConnection connection = new OdbcConnection(ConfigurationManager.ConnectionStrings["MyConStr"].ConnectionString);

            return connection;
        }

        public DataTable ExecuteQueryCommand(string Query)
        {
            IDbConnection connection = getConnection();
            // Ensure that the connection is opened
            ConnectionState originalState = connection.State;
            if (originalState != ConnectionState.Open)
                connection.Open();
            DataTable dataTable = new DataTable();
            try
            {
                IDbCommand command = connection.CreateCommand();
                command.CommandText = Query;

                using (var reader = command.ExecuteReader())
                {
                    dataTable.Load(reader);
                }
                return dataTable;
            }
            finally
            {
                if (originalState == ConnectionState.Closed)
                    connection.Close();
            }
        }

        public int ExecuteNonQueryCommand(string Query)
        {
            IDbConnection connection = getConnection();
            ConnectionState originalState = connection.State;
            if (originalState != ConnectionState.Open)
                connection.Open();
            DataTable dataTable = new DataTable();
            try
            {
                IDbCommand command = connection.CreateCommand();
                command.CommandText = Query;
                int rowsCount = Convert.ToInt32(command.ExecuteScalar());
                return rowsCount;
            }
            finally
            {
                if (originalState == ConnectionState.Closed)
                    connection.Close();
            }
        }
    }
}