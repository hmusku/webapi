//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAPIQA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PublicSurplusSale
    {
        public int psid { get; set; }
        public System.DateTime salesDate { get; set; }
        public decimal sales { get; set; }
    }
}
