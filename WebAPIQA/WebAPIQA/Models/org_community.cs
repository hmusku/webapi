//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAPIQA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class org_community
    {
        public int acct_id { get; set; }
        public string comm_abbr { get; set; }
        public string comm_desc { get; set; }
        public int profile_code { get; set; }
        public string tnc_ind { get; set; }
        public string ep_acct_use { get; set; }
        public Nullable<int> ep_pmt_id { get; set; }
        public string Approval { get; set; }
        public Nullable<int> ApproverPrimary { get; set; }
        public Nullable<int> Approver1 { get; set; }
        public Nullable<int> Approver2 { get; set; }
        public Nullable<int> Approver3 { get; set; }
        public string TierStatus { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<System.DateTime> LastModified_dt { get; set; }
        public Nullable<int> LastModified_user { get; set; }
        public Nullable<decimal> PercentCharged { get; set; }
        public Nullable<decimal> TaxDefault { get; set; }
        public Nullable<bool> PublicView { get; set; }
        public string Requirements { get; set; }
        public string Instructions { get; set; }
        public bool SpecTaxDefault { get; set; }
        public Nullable<bool> shared { get; set; }
        public Nullable<bool> FSS { get; set; }
        public Nullable<int> InspectionInstID { get; set; }
        public Nullable<int> SpecialInstID { get; set; }
        public Nullable<int> RemovalInstID { get; set; }
        public Nullable<int> PaymentInstID { get; set; }
    }
}
