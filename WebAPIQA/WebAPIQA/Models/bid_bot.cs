//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAPIQA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class bid_bot
    {
        public int acct_id { get; set; }
        public int asset_id { get; set; }
        public int buyer_id { get; set; }
        public Nullable<decimal> max_amount { get; set; }
        public Nullable<System.DateTime> bot_date { get; set; }
        public decimal bot_id { get; set; }
        public string auction_ind { get; set; }
        public string ip { get; set; }
    }
}
