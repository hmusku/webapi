﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPIQA.Models
{
    public class QuestionsModel
    {
        public Int32 OrgID { get; set; }
        public Int32 LotID { get; set; }
        public String Question { get; set; }
        public Int32 EMID { get; set; }
        public DateTime? QuestionDateTime { get; set; }
        public Int32 acct_id { get; set; }
        public Int32 asset_id { get; set; }
        public Int32 buyer_id { get; set; }
        public bool copyBuyer { get; set; }
        public int qID { get; set; }
    }

    public class AssetInfo
    {
        public Int32 asset_id { get; set; }
        public String asset_short_desc { get; set; }
        public DateTime asset_auction_end_dt { get; set; }
        public Int32 asset_filler_1 { get; set; }

    }

    public class QuestionModel
    {
        //[Required(ErrorMessage = "OrgID is required.")]
        //public int OrgID { get; set; }
        [Required(ErrorMessage = "LotID is required.")]
        public int LotID { get; set; }
        [Required(ErrorMessage = "Question is required.")]
        public string Question { get; set; }
        [Required(ErrorMessage = "EMID is required.")]
        public int EMID { get; set; }
        [Required(ErrorMessage = "QuestionDateTime is required.")]
        public DateTime? QuestionDateTime { get; set; }
        [Required(ErrorMessage = "copyBuyer is required.")]
        public bool copyBuyer { get; set; }
    }

}