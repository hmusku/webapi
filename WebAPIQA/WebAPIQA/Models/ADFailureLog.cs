//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAPIQA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ADFailureLog
    {
        public long ID { get; set; }
        public string SAM { get; set; }
        public string ADServer { get; set; }
        public bool isHostReachable { get; set; }
        public string Status { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public Nullable<System.DateTime> LogDateTime { get; set; }
    }
}
