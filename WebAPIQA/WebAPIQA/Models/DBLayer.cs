﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Web;

namespace WebAPIQA.Models
{
    class DBLayer
    {
        private static IDbConnection getConnection()
        {
            IDbConnection connection = new OdbcConnection(ConfigurationManager.ConnectionStrings["MyConStr"].ConnectionString);
                       
            return connection;
        }
        //public static Object getScalar(string Query)
        //{
        //    object Result;
        //    OdbcCommand cmd = new OdbcCommand();
        //    try
        //    {
        //        cmd.Connection = getConnection();
        //        cmd.CommandText = Query;
        //        cmd.Connection.Open();
        //        Result = cmd.ExecuteScalar();
        //        return Result;
        //    }
        //    catch (OdbcException SEXC)
        //    {
        //        throw SEXC;
        //    }
        //    finally
        //    {
        //        cmd.Dispose();
        //    }
        //}
        //public static int getNonQuery(string Query)
        //{
        //    int result = -1;
        //    OdbcCommand cmd = new OdbcCommand();
        //    try
        //    {
        //        cmd.Connection = getConnection();
        //        cmd.CommandText = Query;
        //        cmd.Connection.Open();
        //        result = cmd.ExecuteNonQuery();
        //        return result;
        //    }
        //    catch (OdbcException SEXC)
        //    {
        //        throw SEXC;
        //    }
        //    finally
        //    {
        //        cmd.Dispose();
        //    }
        //}
        //public static void ExecuteStoredProcedure(String Query, int argscount, params Object[] param)
        //{
        //    OdbcCommand cmd = new OdbcCommand(Query, getConnection());
        //    try
        //    {
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        for (int i = 0; i < argscount; i++)
        //        {
        //            cmd.Parameters.AddWithValue("sss", param);
        //        }
        //        cmd.ExecuteNonQuery();
        //        // return new object();
        //    }
        //    catch (OdbcException SEXC)
        //    {
        //        throw SEXC;
        //    }
        //}

        //public static DataSet getDataSet(string Query)
        //{
        //    DataSet ds = new DataSet();
        //    OdbcDataAdapter da = new OdbcDataAdapter(Query, getConnection());
        //    try
        //    {
        //        da.Fill(ds);
        //        return ds;
        //    }
        //    catch (OdbcException SEXC)
        //    {
        //        throw SEXC;
        //    }

        //    finally
        //    {
        //        da.Dispose();
        //    }
        //}

        public static DataTable ExecuteQueryCommand(string Query)
        {
            IDbConnection connection = getConnection();
            // Ensure that the connection is opened (otherwise executing the command will fail)
            ConnectionState originalState = connection.State;
            if (originalState != ConnectionState.Open)
                connection.Open();
            DataTable dataTable = new DataTable();
            try
            {
                // Create a command to get the server version
                // NOTE: The query's syntax is SQL Server specific
                IDbCommand command = connection.CreateCommand();
                command.CommandText = Query;

                using (var reader = command.ExecuteReader())
                {
                    dataTable.Load(reader);
                }
                return dataTable;
            }
            finally
            {
                // Close the connection if that's how we got it
                if (originalState == ConnectionState.Closed)
                    connection.Close();
            }
        }

        public static int ExecuteNonQueryCommand(string Query)
        {
            IDbConnection connection = getConnection();
            // Ensure that the connection is opened (otherwise executing the command will fail)
            ConnectionState originalState = connection.State;
            if (originalState != ConnectionState.Open)
                connection.Open();
            DataTable dataTable = new DataTable();
            try
            {
                // Create a command to get the server version
                // NOTE: The query's syntax is SQL Server specific
                IDbCommand command = connection.CreateCommand();
                command.CommandText = Query;
                int rowsCount = Convert.ToInt32(command.ExecuteScalar());
                return rowsCount;
            }
            finally
            {
                // Close the connection if that's how we got it
                if (originalState == ConnectionState.Closed)
                    connection.Close();
            }
        }

    }
}