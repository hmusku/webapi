//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAPIQA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BuyersNote
    {
        public int buyer_id { get; set; }
        public string Note { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> Reminder { get; set; }
        public string ReminderNotify { get; set; }
        public string Type { get; set; }
        public string SubType { get; set; }
        public string Recipients { get; set; }
    }
}
